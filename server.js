const express = require("express");
const { ObjectID } = require("mongodb");
const url = require("url")
const open_url = require('open')
const app = express();
const mongoose = require("mongoose");
const model = require('./model')
require('dotenv').config()
require('dotenv/config');

app.use(express.json())

app.listen(process.env.PORT, () => {
          mongoose.connect(process.env.CONNECTION_URL, { useNewUrlParser: true,useUnifiedTopology: true }, (error, client) => {
              if(error) {
                  throw error;
              }
            });
           });
//===============================Ask Question===================================//      
app.post('/Ask_Question',  async(req,res) => {
       const aldready_asked_question = await model.find({question : req.body.question})
       if(aldready_asked_question.length){
         return res.status(201).send("question already exist")
        }
      const feedback = await model.insertMany({ProductId : req.body.ProductId,Item_code : req.body.Item_code,question : req.body.question, questioned_user_name : req.body.questioned_user_name,status : req.body.status }) ;
         res.status(200).send(feedback)
        })
//===============================post answer===================================//       
app.put('/post_answer/:productId/:questionid', async(req, res, next) => { 
        var check_question = await model.find({ProductId : req.body.ProductId, _id:req.params.questionid})
        if(!check_question){
             return res.status(404)
          }
        if(check_question =! null){
            model.updateMany({ProductId: req.params.productId,_id:req.params.questionid}, {$push: { Answer : req.body.Answer}})
        .then(
              () => {
                res.status(201).json({
                  message: 'Thing updated successfully!'
                });
              }
            ).catch(
              (error) => {
                res.status(400).json({
                  error: error
                });
              }
            );
            }
          });
//=============================get answer based on productId and questionId=================//         
app.get('/get_question/:ProductId/:Item_code/:questionid',async (req,response) => {
         const get_questions = await model.find( { ProductId : req.params.ProductId , _id : req.params.questionid,Item_code: req.params.Item_code })
        .select({
                "ProductId":   true, 
                "Item_code" : true,
                "question": true,
                "status"  : true,
                "user_name" : true,
                "Answer" : true
            }).exec(); 
			console.log(get_quesions)
            response.send(get_questions)
         });
//=============================get answer based on productId==============================//         
app.get('/get_question/:productId/:Item_code',async (req,res) => {
        const get_questions = await model.find({ProductId : req.params.productId,Item_code:req.params.Item_code})
         .select({
                "Answer": true,
                "Item_code" : true,
                "ProductId": true, 
                "question": true,
                "question_id" : true, 
                "Answer_name": true,
            // "Answer_name": true 
            }).exec(); 
            res.send(get_questions)
});
//=========================================Admin panel=================================//

//=========================================Update answer===============================//
app.put('/update_answer/:ProductId/:ansId', async(req,res) => {
           const find_answer_id = await model.find( {"Answer._id" : req.params.ansId })
         if(find_answer_id.length == 0){
            return res.status(404).json({
              message : "answer not found"
            })
          }
        const update_status1 = await model.updateMany({ "Answer._id" : req.params.ansId },{$set : {"Answer.$.Answer_name" : req.body.new_answer,"Answer.$.status" : req.body.status_answer }})
          if(req.body.status_answer == 0 ) {
            const update_status1 = await model.updateMany({$pull : {"Answer": { "status" : req.body.status_answer}}})
          }

          const get_questions = await model.find({ProductId : req.params.ProductId})
         .select({
                "Answer": true,
                "ProductId": true, 
                "question": true,
                "question_id" : true, 
                "Answer_name": true,
                "status":true,
            }).exec(); 
            res.send(get_questions)
         })
//===============================update_question====================================//         
app.put('/update_question/:ProductId/:questionid/', async(req,res) => {
          const find_question_id = await model.findById({_id : req.params.questionid})
          if(find_question_id == null){
             return res.status(404).json({
             message : "question not found"
          });
          }
          const update_question = await model.updateMany({ProductId : req.params.ProductId ,_id : req.params.questionid} ,  {question : req.body.question , status : req.body.status_ques })
         if(req.body.status_ques == 0 ){
            const update_status2 = await model.deleteOne({_id : req.params.questionid},{"status" : req.body.status_ques})
         }
         const get_questions = await model.find({ProductId : req.params.ProductId})
         .select({
                "Answer": true,
                "ProductId": true, 
                "question": true,
                "question_id" : true, 
                "Answer_name": true,
                "status" : true, 
            }).exec(); 
        res.send(get_questions)
         })
//========================================like===================================//
app.put('/updating_like/:user_name/:answer_id', async(req,res)=> {
          const update_array_like_user = await model.find({ "Answer.liked_user.like_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
          const update_array_unlike_user = await model.find({ "Answer.unliked_user.unlike_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
          console.log(update_array_like_user.length)
          if(update_array_like_user.length ==0  && update_array_unlike_user.length == 1 ) {
            const update_status = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.like_count" : 1},$push : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
            const update_status1 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : -1},$pull : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
          } else if(update_array_like_user.length ==0) {
          const update_status3 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.like_count" : 1},$push : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
          }
          res.status(201).json({
          message : "success"
         });
         })
//=====================================unlike===============================//
app.put('/updating_unlike/:user_name/:answer_id', async(req,res)=> {

        const update_array_like_user = await model.find({ "Answer.liked_user.like_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
        const update_array_unlike_user = await model.find({ "Answer.unliked_user.unlike_user" : req.params.user_name , "Answer._id" : req.params.answer_id})
        if(update_array_unlike_user.length == 0 && update_array_like_user.length == 1){
          const update_status1 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : 1},$push : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
          const unlike_user_pull1 = await model.updateMany({ "Answer._id" : req.params.answer_id },{$inc : {"Answer.$.like_count" : -1}, $pull : {"Answer.$.liked_user": { "like_user" : req.params.user_name}}})
        } else  if(update_array_unlike_user.length == 0){
          const update_status12 = await model.updateMany({Answer : {$elemMatch : { _id : req.params.answer_id}}},{$inc : {"Answer.$.unlike_count" : 1},$push : {"Answer.$.unliked_user": { "unlike_user" : req.params.user_name}}})
          res.status(201).json({
        message : "ok"
      });
      }

    })
//=================================delete====================================//      
app.delete('/delete', async(req,res) => {
       const deleting_question = await model.deleteMany({})
       res.send(deleting_question)
     })
console.log("its is working " +process.env.PORT )
    

